'use strict';
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as enumerator from "./enumerator";

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "enumerator" is now active!');

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with  registerCommand
	// The commandId parameter must match the command field in package.json
	let disposable = vscode.commands.registerTextEditorCommand('extension.enumerate', (textEditor: vscode.TextEditor) => {
		
        let IBoxOptions : vscode.InputBoxOptions = {
            prompt:'<cmd> <repeat_text #> [step] [x-arg]',
            placeHolder: 'as row-1 2',
            password: false
        };
        
        vscode.window.showInputBox(IBoxOptions).then((inputPattern) => {
            if (!inputPattern || inputPattern == "") {
                return; //no pattern
            } 
            
            let inputPieces = inputPattern.split(' ');
            let command = inputPieces.shift();
            let commandParameters = inputPieces.join(' ');
            
            switch (command) {
                case "as": 
                    enumerator.autoStep(textEditor, commandParameters);
                    break;
                
                case "uuid":
                    enumerator.insertUuid(textEditor, commandParameters, false);
                    break;
                    
                case "UUID": 
                    enumerator.insertUuid(textEditor, commandParameters, true);
                    break;
                   
                default:
                    vscode.window.showInformationMessage(command + " command not supported");
                    break;
            }
        });
	});

	context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() {
}